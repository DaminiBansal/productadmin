from django.shortcuts import render,redirect
from django.contrib.auth import  login as auth_login , authenticate
from .tables import ProductTable
from .models import forecast_table,promotions_product,UpdateLog_Master,Product_Master,Store_Master,ThematicPromotions_Master,TacticalPromotions_Master,InternalPromotions_Master,Events_Master

from .assume_quicksight_role import assumed_session
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_control

from django.http import HttpResponse, HttpResponseNotFound
from django.contrib import messages
import xlwt
from django.template import RequestContext
from django.contrib.auth.models import User
import os
import urllib
import urllib.request
import json
import urllib.request
import botocore.session
from datetime import datetime
from django_datatables_view.base_datatable_view import BaseDatatableView

def handler404(request):
    response = render(request,"src/404.html",status=404)
    return response


def handler500(request):
    response = render(request,"src/500.html",status=500)
    return response

@login_required(login_url='/accounts/login')
def awsdashboard(request):
    #session = botocore.session.get_session()
    #client = session.create_client("quicksight", region_name='us-east-1')
    #url=client.get_dashboard_embed_url(AwsAccountId="355833338907", DashboardId="8e5c9503-54fe-46cc-950c-cb86fc498047", IdentityType="IAM", SessionLifetimeInMinutes=100, ResetDisabled=True, UndoRedoDisabled=True)

    #print(url)
    #embed_url = url['EmbedUrl']
    embed_url = assumed_session("arn:aws:iam::355833338907:role/quicksightembedrole",'Urvi')
    print(embed_url)
    return render(request,"quicksighttest.html",{'embed_url':embed_url})

# Create your views here.
def login(request):
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')
    user = authenticate(username = username, password = password)
    if user is not None:
        auth_login(request, user)
        recaptcha_response = request.POST.get('g-recaptcha-response')
        url = 'https://www.google.com/recaptcha/api/siteverify'
        values = {
            'secret': os.environ['GOOGLE_RECAPTCHA_SECRET_KEY'],
            'response': recaptcha_response
        }
        data = urllib.parse.urlencode(values).encode()
        req =  urllib.request.Request(url, data=data)
        response = urllib.request.urlopen(req)
        result = json.loads(response.read().decode())
        if result['success']:
            return redirect('/')
        else:
            return render(request, 'registration/login.html')
    else:
        return render(request, 'registration/login.html')

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def addUser(request):
    enableUserFun = False
    if str(request.user) == str(os.environ['ADMIN']):
        enableUserFun = True
    return render(request,'src/adduser.html', {'userlist':User.objects.all(),'userobj':None,'enableUserFun':enableUserFun,'category_name':getcategoryname(request.user)})

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def saveNewUser(request):
    username = request.POST.get('username','')
    if User.objects.filter(username=username).count()>0:
        user = User.objects.get(username=username)
    else:
        user = User()
    user.username = username
    user.set_password(request.POST.get('password',''))
    user.first_name = request.POST.get('firstname','')
    user.last_name = request.POST.get('lastname','')
    user.email = request.POST.get('emailaddress','')
    try:
        if str(request.user) == str(os.environ['ADMIN']):
            user.save()
            if User.objects.filter(username=username).count()>0:
                messages.success(request,"User updated successfully")
            else:
                message.success(request,"New user added successfully")
    except:
        messages.success(request,"username had already been taken")
    return redirect('/add-user/')

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def deleteuser(request,username):
    userobj = User.objects.get(username=username)
    try:
        if str(request.user) == str(os.environ['ADMIN']):
            userobj.delete()
            messages.success(request,"User deleted successfully")
    except:
        messages.success(request,"Error in deleting user")
    return redirect('/add-user/')

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def edituser(request,username):
    enableUserFun = False
    if str(request.user) == str(os.environ['ADMIN']):
        enableUserFun = True
    return render(request,'src/adduser.html', {'category_name':getcategoryname(request.user),'userlist':User.objects.all(),'userobj':User.objects.get(username=username),'enableUserFun':enableUserFun})

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def storeDetail(request):    
    storeslist = Store_Master.objects.all()
    active = Product_Master.objects.filter(ACTIVE_FLAG__iexact="Yes").count() 
    inactive = Product_Master.objects.filter(ACTIVE_FLAG__iexact="No").count()
    nostatus = Product_Master.objects.filter(ACTIVE_FLAG=None).count() + Product_Master.objects.filter(ACTIVE_FLAG="").count()+Product_Master.objects.filter(ACTIVE_FLAG__iexact="#N/A").count()
    endoflife = Product_Master.objects.filter(CUSTOM_CATEGORY11__iexact="Yes").count()
    return render(request, 'src/home.html',{'storelist':storeslist,'active':active,'inactive':inactive,'nostatus':nostatus,
        'endoflife':endoflife,'category_name':getcategoryname(request.user)})


@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def storeEditForm(request,storeid):
    storeDetail = Store_Master.objects.get(SITE_ID=storeid)
    return render(request, 'src/store-editform.html',{'storeDetail':storeDetail,'category_name':getcategoryname(request.user)})


@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def updateStoreDetails(request, storeid):     
    store = Store_Master.objects.get(SITE_ID=storeid)
    store.CHANNEL_ID     = request.POST.get('channelid', '')
    store.CHANNEL        = request.POST.get('channel', '')
    store.STORE_PROVINCE = request.POST.get('province', '')
    store.DISTRICT       = request.POST.get('district', '')
    store.TYPE           = request.POST.get('type', '')
    store.LATTITUDE      = request.POST.get('latitude', '')
    store.LONGITUDE      = request.POST.get('longitude', '')
    store.MOBILE_NO      = request.POST.get('mobileno','')
    store.save()
    storelist = Store_Master.objects.all()
    return render(request, 'src/home.html',{'storelist':storelist,'category_name':getcategoryname(request.user)})

 
@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def refrigeratorEditForm(request,productId,productcategory):
    productDetail = Product_Master.objects.get(PRODUCT_CODE=productId)
    return render(request, 'src/refrigerator-editform.html',{'replacement_code_list':getreplacementcodelist(productcategory),'productDetail':productDetail,'productdetail':'active','category_name':getcategoryname(request.user),'productcategory':productcategory})


@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def updateRefrigeratorDetails(request, productcode):     
    product = Product_Master.objects.get(PRODUCT_CODE=productcode)
    product.CUSTOM_CATEGORY1 = request.POST.get('customcategory1', '') 
    product.CUSTOM_CATEGORY2 = request.POST.get('customcategory2', '') 
    product.CUSTOM_CATEGORY3 = request.POST.get('customcategory3', '') 
    product.CUSTOM_CATEGORY4 = request.POST.get('customcategory4', '') 
    product.CUSTOM_CATEGORY5 = request.POST.get('customcategory5', '') 
    product.CUSTOM_CATEGORY6 = request.POST.get('customcategory6', '') 
    product.CUSTOM_CATEGORY11 = request.POST.get('customcategory11', '')
    product.ACTIVE_FLAG       = request.POST.get('active','')
    product.save()
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='Refrigerators')
    return redirect('/product-details/Refrigerators/')


@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def televisionEditForm(request,productId,productcategory):
    productDetail = Product_Master.objects.get(PRODUCT_CODE=productId)
    return render(request, 'src/television-editform.html',{'replacement_code_list':getreplacementcodelist(productcategory),'productDetail':productDetail,'productdetail':'active','category_name':getcategoryname(request.user),'productcategory':productcategory})


@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def updateTelevisionDetails(request, productcode):     
    product = Product_Master.objects.get(PRODUCT_CODE=productcode)
    product.CUSTOM_CATEGORY1 = request.POST.get('customcategory1', '') 
    product.CUSTOM_CATEGORY2 = request.POST.get('customcategory2', '') 
    product.CUSTOM_CATEGORY3 = request.POST.get('customcategory3', '') 
    product.CUSTOM_CATEGORY4 = request.POST.get('customcategory4', '') 
    product.CUSTOM_CATEGORY5 = request.POST.get('customcategory5', '') 
    product.CUSTOM_CATEGORY6 = request.POST.get('customcategory6', '') 
    product.CUSTOM_CATEGORY11 = request.POST.get('customcategory11', '')
    product.ACTIVE_FLAG       = request.POST.get('active','')
    product.save()
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__in=['TELEVISIONS - LED','TELEVISION - LCD & PLASMA','TELEVISIONS - CRT'])
    return redirect('/product-details/Television/')

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def kitchenappliancesEditForm(request,productId,productcategory):
    productDetail = Product_Master.objects.get(PRODUCT_CODE=productId)
    return render(request, 'src/kitchenappliances-editform.html',{'replacement_code_list':getreplacementcodelist(productcategory),'productDetail':productDetail,'productdetail':'active','category_name':getcategoryname(request.user),'productcategory':productcategory})

 
@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def updateKitchenappliancesDetails(request, productcode):     
    product = Product_Master.objects.get(PRODUCT_CODE=productcode)
    product.CUSTOM_CATEGORY1 = request.POST.get('customcategory1', '') 
    product.CUSTOM_CATEGORY2 = request.POST.get('customcategory2', '') 
    product.CUSTOM_CATEGORY11 = request.POST.get('customcategory11', '')
    product.ACTIVE_FLAG       = request.POST.get('active','')
    product.save()
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='KITCHEN APPLANCES')
    return redirect('/product-details/KITCHEN APPLANCES/')

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def airconditionerEditForm(request,productId,productcategory):
    productDetail = Product_Master.objects.get(PRODUCT_CODE=productId)
    return render(request, 'src/airconditioner-editform.html',{'replacement_code_list':getreplacementcodelist(productcategory),'productDetail':productDetail,'productdetail':'active','category_name':getcategoryname(request.user),'productcategory':productcategory})

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def updateairconditionerDetails(request, productcode):     
    product = Product_Master.objects.get(PRODUCT_CODE=productcode)
    product.CUSTOM_CATEGORY1 = request.POST.get('customcategory1', '') 
    product.CUSTOM_CATEGORY2 = request.POST.get('customcategory2', '')
    product.CUSTOM_CATEGORY3 = request.POST.get('customcategory3', '')
    product.CUSTOM_CATEGORY4 = request.POST.get('customcategory4', '')
    product.CUSTOM_CATEGORY11 = request.POST.get('customcategory11', '')
    product.ACTIVE_FLAG       = request.POST.get('active','')
    product.save()
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='AIR CONDITIONERS')
    return redirect('/product-details/AIR CONDITIONERS/')


@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def fanEditForm(request,productId,productcategory):
    productDetail = Product_Master.objects.get(PRODUCT_CODE=productId)
    return render(request, 'src/fan-editform.html',{'replacement_code_list':getreplacementcodelist(productcategory),'productDetail':productDetail,'productdetail':'active','category_name':getcategoryname(request.user),'productcategory':productcategory})

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def updatefanDetails(request, productcode):     
    product = Product_Master.objects.get(PRODUCT_CODE=productcode)
    product.CUSTOM_CATEGORY1 = request.POST.get('customcategory1', '') 
    product.CUSTOM_CATEGORY2 = request.POST.get('customcategory2', '')
    product.CUSTOM_CATEGORY3 = request.POST.get('customcategory3', '')
    product.CUSTOM_CATEGORY11 = request.POST.get('customcategory11', '')
    product.ACTIVE_FLAG       = request.POST.get('active','')
    product.save()
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='FANS')
    return redirect('/product-details/FANS/')

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def ironEditForm(request,productId,productcategory):
    productDetail = Product_Master.objects.get(PRODUCT_CODE=productId)
    return render(request, 'src/iron-editform.html',{'replacement_code_list':getreplacementcodelist(productcategory),'productDetail':productDetail,'productdetail':'active','category_name':getcategoryname(request.user),'productcategory':productcategory})


@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def updateironDetails(request, productcode):     
    product = Product_Master.objects.get(PRODUCT_CODE=productcode)
    product.CUSTOM_CATEGORY1 = request.POST.get('customcategory1', '') 
    product.CUSTOM_CATEGORY2 = request.POST.get('customcategory2', '')
    product.CUSTOM_CATEGORY3 = request.POST.get('customcategory3', '')
    product.CUSTOM_CATEGORY4 = request.POST.get('customcategory4', '')
    product.CUSTOM_CATEGORY11 = request.POST.get('customcategory11', '')
    product.ACTIVE_FLAG       = request.POST.get('active','')
    product.save()
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='IRONS')
    return redirect('/product-details/IRONS/')

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def ricecookerEditForm(request,productId,productcategory):
    productDetail = Product_Master.objects.get(PRODUCT_CODE=productId)
    return render(request, 'src/ricecooker-editform.html',{'replacement_code_list':getreplacementcodelist(productcategory),'productDetail':productDetail,'productdetail':'active','category_name':getcategoryname(request.user),'productcategory':productcategory})


@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def updatericecookerDetails(request, productcode):     
    product = Product_Master.objects.get(PRODUCT_CODE=productcode)
    product.CUSTOM_CATEGORY1 = request.POST.get('customcategory1', '') 
    product.CUSTOM_CATEGORY2 = request.POST.get('customcategory2', '')
    product.CUSTOM_CATEGORY3 = request.POST.get('customcategory3', '')
    product.CUSTOM_CATEGORY4 = request.POST.get('customcategory4', '')
    product.CUSTOM_CATEGORY5 = request.POST.get('customcategory5', '')
    product.CUSTOM_CATEGORY11 = request.POST.get('customcategory11', '')
    product.ACTIVE_FLAG       = request.POST.get('active','') 
    product.save()
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='RICE COOKERS')
    return redirect('/product-details/RICE COOKERS/')

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def audioEditForm(request,productId,productcategory):
    productDetail = Product_Master.objects.get(PRODUCT_CODE=productId)
    return render(request, 'src/audio-editform.html',{'replacement_code_list':getreplacementcodelist(productcategory),'productDetail':productDetail,'productdetail':'active','category_name':getcategoryname(request.user),'productcategory':productcategory})


@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def updateaudioDetails(request, productcode):     
    product = Product_Master.objects.get(PRODUCT_CODE=productcode)
    product.CUSTOM_CATEGORY1 = request.POST.get('customcategory1', '') 
    product.CUSTOM_CATEGORY2 = request.POST.get('customcategory2', '')
    product.CUSTOM_CATEGORY3 = request.POST.get('customcategory3', '')
    product.CUSTOM_CATEGORY4 = request.POST.get('customcategory4', '')
    product.CUSTOM_CATEGORY5 = request.POST.get('customcategory5', '')
    product.CUSTOM_CATEGORY6 = request.POST.get('customcategory6', '')
    product.CUSTOM_CATEGORY7 = request.POST.get('customcategory7', '')
    product.CUSTOM_CATEGORY11 = request.POST.get('customcategory11', '')
    product.ACTIVE_FLAG       = request.POST.get('active','') 
    product.save()
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='AUDIOS')
    return redirect('/product-details/AUDIOS/')

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def sewingmachineEditForm(request,productId,productcategory):
    productDetail = Product_Master.objects.get(PRODUCT_CODE=productId)
    return render(request, 'src/sewingmachine-editform.html',{'replacement_code_list':getreplacementcodelist(productcategory),'productDetail':productDetail,'productdetail':'active','category_name':getcategoryname(request.user),'productcategory':productcategory})


@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def updatesewingmachineDetails(request, productcode):     
    product = Product_Master.objects.get(PRODUCT_CODE=productcode)
    product.CUSTOM_CATEGORY1 = request.POST.get('customcategory1', '') 
    product.CUSTOM_CATEGORY2 = request.POST.get('customcategory2', '')
    product.CUSTOM_CATEGORY3 = request.POST.get('customcategory3', '')
    product.CUSTOM_CATEGORY4 = request.POST.get('customcategory4', '')
    product.CUSTOM_CATEGORY5 = request.POST.get('customcategory5', '')
    product.CUSTOM_CATEGORY6 = request.POST.get('customcategory6', '')
    product.CUSTOM_CATEGORY11 = request.POST.get('customcategory11', '')
    product.ACTIVE_FLAG       = request.POST.get('active','')
    product.save()
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='SEWING MACHINES')
    return redirect('/product-details/SEWING MACHINES/')

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def washingmachineEditForm(request,productId,productcategory):
    productDetail = Product_Master.objects.get(PRODUCT_CODE=productId)
    return render(request, 'src/washingmachine-editform.html',{'replacement_code_list':getreplacementcodelist(productcategory),'productDetail':productDetail,'productdetail':'active','category_name':getcategoryname(request.user),'productcategory':productcategory})


@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def updatewashingmachineDetails(request, productcode):     
    product = Product_Master.objects.get(PRODUCT_CODE=productcode)
    product.CUSTOM_CATEGORY1 = request.POST.get('customcategory1', '') 
    product.CUSTOM_CATEGORY2 = request.POST.get('customcategory2', '')    
    product.CUSTOM_CATEGORY3 = request.POST.get('customcategory3', '')
    product.CUSTOM_CATEGORY4 = request.POST.get('customcategory4', '')
    product.CUSTOM_CATEGORY5 = request.POST.get('customcategory5', '')
    product.CUSTOM_CATEGORY6 = request.POST.get('customcategory6', '')
    product.CUSTOM_CATEGORY11 = request.POST.get('customcategory11', '')
    product.ACTIVE_FLAG       = request.POST.get('active','') 
    product.save()
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='WASHING MACHINE')
    return redirect('/product-details/WASHING MACHINE/')

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def productDetail(request,productcategory):
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact=productcategory)
    if productcategory=="Refrigerators":
        return render(request, 'src/refrigerator-datatable.html',{'category_name':getcategoryname(request.user),'products':products,'active':getActiveProductsCount(productcategory),'inactive':getInActiveProductsCount(productcategory),'nostatus':getNoStatusProductsCount(productcategory),'endoflife':getEndOfLifeProductsCount(productcategory),'productcategory':productcategory,'productdetail':'active'})
    elif productcategory=="Television":
        products = Product_Master.objects.filter(PROD_LEVEL2_DESC__in=['TELEVISIONS - LED','TELEVISION - LCD & PLASMA','TELEVISIONS - CRT'])
        return render(request, 'src/television-datatable.html',{'category_name':getcategoryname(request.user),'products':products,'active':getActiveProductsCount(productcategory),'inactive':getInActiveProductsCount(productcategory),'nostatus':getNoStatusProductsCount(productcategory),'endoflife':getEndOfLifeProductsCount(productcategory),'productcategory':productcategory,'productdetail':'active'})
    elif productcategory=="KITCHEN APPLANCES":
        return render(request, 'src/kitchenappliances-datatable.html',{'category_name':getcategoryname(request.user),'products':products,'active':getActiveProductsCount(productcategory),'inactive':getInActiveProductsCount(productcategory),'nostatus':getNoStatusProductsCount(productcategory),'endoflife':getEndOfLifeProductsCount(productcategory),'productcategory':productcategory,'productdetail':'active'})
    elif productcategory=="AIR CONDITIONERS":
        return render(request, 'src/airconditioner-datatable.html',{'category_name':getcategoryname(request.user),'products':products,'active':getActiveProductsCount(productcategory),'inactive':getInActiveProductsCount(productcategory),'nostatus':getNoStatusProductsCount(productcategory),'endoflife':getEndOfLifeProductsCount(productcategory),'productcategory':productcategory,'productdetail':'active'})
    elif productcategory=="FANS":
        return render(request, 'src/fan-datatable.html',{'category_name':getcategoryname(request.user),'products':products,'active':getActiveProductsCount(productcategory),'inactive':getInActiveProductsCount(productcategory),'nostatus':getNoStatusProductsCount(productcategory),'endoflife':getEndOfLifeProductsCount(productcategory),'productcategory':productcategory,'productdetail':'active'})
    elif productcategory=="IRONS":
        return render(request, 'src/iron-datatable.html',{'category_name':getcategoryname(request.user),'products':products,'active':getActiveProductsCount(productcategory),'inactive':getInActiveProductsCount(productcategory),'nostatus':getNoStatusProductsCount(productcategory),'endoflife':getEndOfLifeProductsCount(productcategory),'productcategory':productcategory,'productdetail':'active'})
    elif productcategory=="RICE COOKERS":
        return render(request, 'src/ricecooker-datatable.html',{'category_name':getcategoryname(request.user),'products':products,'active':getActiveProductsCount(productcategory),'inactive':getInActiveProductsCount(productcategory),'nostatus':getNoStatusProductsCount(productcategory),'endoflife':getEndOfLifeProductsCount(productcategory),'productcategory':productcategory,'productdetail':'active'})
    elif productcategory=="AUDIOS":
        return render(request, 'src/audio-datatable.html',{'category_name':getcategoryname(request.user),'products':products,'active':getActiveProductsCount(productcategory),'inactive':getInActiveProductsCount(productcategory),'nostatus':getNoStatusProductsCount(productcategory),'endoflife':getEndOfLifeProductsCount(productcategory),'productcategory':productcategory,'productdetail':'active'})
    elif productcategory=="SEWING MACHINES":
        return render(request, 'src/sewingmachine-datatable.html',{'category_name':getcategoryname(request.user),'products':products,'active':getActiveProductsCount(productcategory),'inactive':getInActiveProductsCount(productcategory),'nostatus':getNoStatusProductsCount(productcategory),'endoflife':getEndOfLifeProductsCount(productcategory),'productcategory':productcategory,'productdetail':'active'})
    elif productcategory=="WASHING MACHINE":
        return render(request, 'src/washingmachine-datatable.html',{'category_name':getcategoryname(request.user),'products':products,'active':getActiveProductsCount(productcategory),'inactive':getInActiveProductsCount(productcategory),'nostatus':getNoStatusProductsCount(productcategory),'endoflife':getEndOfLifeProductsCount(productcategory),'productcategory':productcategory,'productdetail':'active'})
    else:
        return render(request, 'src/waterpump-datatable.html',{'category_name':getcategoryname(request.user),'products':products,'active':getActiveProductsCount(productcategory),'inactive':getInActiveProductsCount(productcategory),'nostatus':getNoStatusProductsCount(productcategory),'endoflife':getEndOfLifeProductsCount(productcategory),'productcategory':productcategory,'productdetail':'active'})

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def waterpumpEditForm(request,productId,productcategory):
    productDetail = Product_Master.objects.get(PRODUCT_CODE=productId)
    return render(request, 'src/waterpump-editform.html',{'replacement_code_list':getreplacementcodelist(productcategory),'category_name':getcategoryname(request.user),'productDetail':productDetail,'productdetail':'active','productcategory':productcategory})


@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def updatewaterpumpDetails(request, productcode):     
    product = Product_Master.objects.get(PRODUCT_CODE=productcode)
    product.CUSTOM_CATEGORY1 = request.POST.get('customcategory1', '') 
    product.CUSTOM_CATEGORY2 = request.POST.get('customcategory2', '')
    product.CUSTOM_CATEGORY3 = request.POST.get('customcategory3', '')
    product.CUSTOM_CATEGORY4 = request.POST.get('customcategory4', '')
    product.CUSTOM_CATEGORY11 = request.POST.get('customcategory11', '')
    product.ACTIVE_FLAG       = request.POST.get('active','') 
    product.save()
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='WATER PUMPS')
    return redirect('/product-details/WATER PUMPS/')

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def export_refrigerator_productdetails_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "RefrigeratorsProductDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Refrigerators Product Details')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Code', 'Product Description', 'Brand Description', 'Capacity(in ltrs)', 'Cooling Type', 'No of Doors', 
               'Inverter', 'Color','Existing Modes/Replacement Code','End of Life','Active']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='Refrigerators')
    for product in products:
        row_num += 1
        ws.write(row_num, 0, product.PRODUCT_CODE)
        ws.write(row_num, 1, product.PRODUCT_DESCRIPTION)
        ws.write(row_num, 2, product.BRAND_LEVEL2_DESC)
        ws.write(row_num, 3, product.CUSTOM_CATEGORY1)
        ws.write(row_num, 4, product.CUSTOM_CATEGORY2)
        ws.write(row_num, 5, product.CUSTOM_CATEGORY3)
        ws.write(row_num, 6, product.CUSTOM_CATEGORY4)
        ws.write(row_num, 7, product.CUSTOM_CATEGORY5)
        ws.write(row_num, 8, product.CUSTOM_CATEGORY6)
        ws.write(row_num, 9, product.CUSTOM_CATEGORY11)
        ws.write(row_num, 10, product.ACTIVE_FLAG)

    wb.save(response)
    return response

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def export_television_productdetails_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "TelevisionsProductDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Television Product Details')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Code', 'Product Description', 'Brand Description', 'Size(in inchs)', 'Smart', 'Resolution', 
               'Operating System', 'Shape','Existing Modes/Replacement Code','End of Life','Active']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__in=['TELEVISIONS - LED','TELEVISION - LCD & PLASMA','TELEVISIONS - CRT'])
    for product in products:
        row_num += 1
        ws.write(row_num, 0, product.PRODUCT_CODE)
        ws.write(row_num, 1, product.PRODUCT_DESCRIPTION)
        ws.write(row_num, 2, product.BRAND_LEVEL2_DESC)
        ws.write(row_num, 3, product.CUSTOM_CATEGORY1)
        ws.write(row_num, 4, product.CUSTOM_CATEGORY2)
        ws.write(row_num, 5, product.CUSTOM_CATEGORY3)
        ws.write(row_num, 6, product.CUSTOM_CATEGORY4)
        ws.write(row_num, 7, product.CUSTOM_CATEGORY5)
        ws.write(row_num, 8, product.CUSTOM_CATEGORY6)
        ws.write(row_num, 9, product.CUSTOM_CATEGORY11)
        ws.write(row_num, 10, product.ACTIVE_FLAG)

    wb.save(response)
    return response

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def export_kitchenappliance_productdetails_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "KitchenAppliancesProductDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Kitchen-Appliance Products')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Code', 'Product Description', 'Brand Description', 'SubCategory','Existing Modes/Replacement Code','End of Life','Active']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='KITCHEN APPLANCES')
    for product in products:
        row_num += 1
        ws.write(row_num, 0, product.PRODUCT_CODE)
        ws.write(row_num, 1, product.PRODUCT_DESCRIPTION)
        ws.write(row_num, 2, product.BRAND_LEVEL2_DESC)
        ws.write(row_num, 3, product.CUSTOM_CATEGORY1)
        ws.write(row_num, 4, product.CUSTOM_CATEGORY2)
        ws.write(row_num, 5, product.CUSTOM_CATEGORY11)
        ws.write(row_num, 6, product.ACTIVE_FLAG)

    wb.save(response)
    return response

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def export_airconditioner_productdetails_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "AirConditionersProductDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Air-Conditioner Products')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Code', 'Product Description', 'Brand Description', 'Type', 'Inverter', 'Capacity(BTU wise)', 
               'Existing Modes/Replacement Code','End of Life','Active']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='AIR CONDITIONERS')
    for product in products:
        row_num += 1
        ws.write(row_num, 0, product.PRODUCT_CODE)
        ws.write(row_num, 1, product.PRODUCT_DESCRIPTION)
        ws.write(row_num, 2, product.BRAND_LEVEL2_DESC)
        ws.write(row_num, 3, product.CUSTOM_CATEGORY1)
        ws.write(row_num, 4, product.CUSTOM_CATEGORY2)
        ws.write(row_num, 5, product.CUSTOM_CATEGORY3)
        ws.write(row_num, 6, product.CUSTOM_CATEGORY4)
        ws.write(row_num, 7, product.CUSTOM_CATEGORY11)
        ws.write(row_num, 8, product.ACTIVE_FLAG)

    wb.save(response)
    return response

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def export_fan_productdetails_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "FansProductDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Fans Product Details')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Code', 'Product Description', 'Brand Description', 'Use', 'Type', 'Existing Modes/Replacement Code','End of Life','Active']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='FANS')
    for product in products:
        row_num += 1
        ws.write(row_num, 0, product.PRODUCT_CODE)
        ws.write(row_num, 1, product.PRODUCT_DESCRIPTION)
        ws.write(row_num, 2, product.BRAND_LEVEL2_DESC)
        ws.write(row_num, 3, product.CUSTOM_CATEGORY1)
        ws.write(row_num, 4, product.CUSTOM_CATEGORY2)
        ws.write(row_num, 5, product.CUSTOM_CATEGORY3)
        ws.write(row_num, 6, product.CUSTOM_CATEGORY11)
        ws.write(row_num, 7, product.ACTIVE_FLAG)

    wb.save(response)
    return response

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def export_iron_productdetails_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "IronsProductDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Irons Product Details')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Code', 'Product Description', 'Brand Description', 'Weight', 'Functionality', 'Sole Plate', 
               'Existing Modes/Replacement Code','End of Life','Active']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='IRONS')
    for product in products:
        row_num += 1
        ws.write(row_num, 0, product.PRODUCT_CODE)
        ws.write(row_num, 1, product.PRODUCT_DESCRIPTION)
        ws.write(row_num, 2, product.BRAND_LEVEL2_DESC)
        ws.write(row_num, 3, product.CUSTOM_CATEGORY1)
        ws.write(row_num, 4, product.CUSTOM_CATEGORY2)
        ws.write(row_num, 5, product.CUSTOM_CATEGORY3)
        ws.write(row_num, 6, product.CUSTOM_CATEGORY4)
        ws.write(row_num, 7, product.CUSTOM_CATEGORY11)
        ws.write(row_num, 8, product.ACTIVE_FLAG)

    wb.save(response)
    return response

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def export_ricecooker_productdetails_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "RiceCookersProductDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Rice Cooker Products')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Code', 'Product Description', 'Brand Description', 'Capacity(in ltrs)', 'Type', 'Multicooker', 
               'Color','Existing Modes/Replacement Code','End of Life','Active']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='RICE COOKERS')
    for product in products:
        row_num += 1
        ws.write(row_num, 0, product.PRODUCT_CODE)
        ws.write(row_num, 1, product.PRODUCT_DESCRIPTION)
        ws.write(row_num, 2, product.BRAND_LEVEL2_DESC)
        ws.write(row_num, 3, product.CUSTOM_CATEGORY1)
        ws.write(row_num, 4, product.CUSTOM_CATEGORY2)
        ws.write(row_num, 5, product.CUSTOM_CATEGORY3)
        ws.write(row_num, 6, product.CUSTOM_CATEGORY4)
        ws.write(row_num, 7, product.CUSTOM_CATEGORY5)
        ws.write(row_num, 8, product.CUSTOM_CATEGORY11)
        ws.write(row_num, 9, product.ACTIVE_FLAG)

    wb.save(response)
    return response

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def export_audio_productdetails_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "AudiosProductDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Audios Product Details')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Code', 'Product Description', 'Brand Description', 'Subcategory', 'Tower', 'Bluetooth', 
               'USB','CD/DVD','Wattage(Value)','Existing Modes/Replacement Code','End of Life','Active']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='AUDIOS')
    for product in products:
        row_num += 1
        ws.write(row_num, 0, product.PRODUCT_CODE)
        ws.write(row_num, 1, product.PRODUCT_DESCRIPTION)
        ws.write(row_num, 2, product.BRAND_LEVEL2_DESC)
        ws.write(row_num, 3, product.CUSTOM_CATEGORY1)
        ws.write(row_num, 4, product.CUSTOM_CATEGORY2)
        ws.write(row_num, 5, product.CUSTOM_CATEGORY3)
        ws.write(row_num, 6, product.CUSTOM_CATEGORY4)
        ws.write(row_num, 7, product.CUSTOM_CATEGORY5)
        ws.write(row_num, 8, product.CUSTOM_CATEGORY6)
        ws.write(row_num, 9, product.CUSTOM_CATEGORY7)
        ws.write(row_num, 10, product.CUSTOM_CATEGORY11)
        ws.write(row_num, 11, product.ACTIVE_FLAG)

    wb.save(response)
    return response

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def export_sewingmachine_productdetails_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "SewingMachinesProductDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Sewing Machine Products')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Code', 'Product Description', 'Brand Description', 'Type', 'Stitch Type', 'Hi-Tech', 
               'Use','Weight','Existing Modes/Replacement Code','End of Life','Active']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='SEWING MACHINES')
    for product in products:
        row_num += 1
        ws.write(row_num, 0, product.PRODUCT_CODE)
        ws.write(row_num, 1, product.PRODUCT_DESCRIPTION)
        ws.write(row_num, 2, product.BRAND_LEVEL2_DESC)
        ws.write(row_num, 3, product.CUSTOM_CATEGORY1)
        ws.write(row_num, 4, product.CUSTOM_CATEGORY2)
        ws.write(row_num, 5, product.CUSTOM_CATEGORY3)
        ws.write(row_num, 6, product.CUSTOM_CATEGORY4)
        ws.write(row_num, 7, product.CUSTOM_CATEGORY5)
        ws.write(row_num, 8, product.CUSTOM_CATEGORY6)
        ws.write(row_num, 9, product.CUSTOM_CATEGORY11)
        ws.write(row_num, 10, product.ACTIVE_FLAG)

    wb.save(response)
    return response

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def export_washingmachine_productdetails_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "WashingMachinesProductDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Washing Machine Products')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Code', 'Product Description', 'Brand Description', 'Capacity in Litres', 'Automatic Type', 'Load', 
               'Inverter','Color','Existing Modes/Replacement Code','End of Life','Active']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='WASHING MACHINE')
    for product in products:
        row_num += 1
        ws.write(row_num, 0, product.PRODUCT_CODE)
        ws.write(row_num, 1, product.PRODUCT_DESCRIPTION)
        ws.write(row_num, 2, product.BRAND_LEVEL2_DESC)
        ws.write(row_num, 3, product.CUSTOM_CATEGORY1)
        ws.write(row_num, 4, product.CUSTOM_CATEGORY2)
        ws.write(row_num, 5, product.CUSTOM_CATEGORY3)
        ws.write(row_num, 6, product.CUSTOM_CATEGORY4)
        ws.write(row_num, 7, product.CUSTOM_CATEGORY5)
        ws.write(row_num, 8, product.CUSTOM_CATEGORY6)
        ws.write(row_num, 9, product.CUSTOM_CATEGORY11)
        ws.write(row_num, 10, product.ACTIVE_FLAG)

    wb.save(response)
    return response

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def export_waterpump_productdetails_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "WaterPumpsProductDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Water Pumps Product Details')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Code', 'Product Description', 'Brand Description', 'Category', 'Pump Type', 'Submersible Type', 
               'Horse Power Rating','Pipes','Factory vs Direct Imports','Country of Origin of Motor','Existing Modes/Replacement Code','End of Life','Active']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    products = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact='WATER PUMPS')
    for product in products:
        row_num += 1
        ws.write(row_num, 0, product.PRODUCT_CODE)
        ws.write(row_num, 1, product.PRODUCT_DESCRIPTION)
        ws.write(row_num, 2, product.BRAND_LEVEL2_DESC)
        ws.write(row_num, 3, product.CUSTOM_CATEGORY1)
        ws.write(row_num, 4, product.CUSTOM_CATEGORY2)
        ws.write(row_num, 5, product.CUSTOM_CATEGORY3)
        ws.write(row_num, 6, product.CUSTOM_CATEGORY4)
        ws.write(row_num, 7, product.CUSTOM_CATEGORY5)
        ws.write(row_num, 8, product.CUSTOM_CATEGORY6)        
        ws.write(row_num, 9, product.CUSTOM_CATEGORY7)
        ws.write(row_num, 10, product.CUSTOM_CATEGORY11)
        ws.write(row_num, 11, product.ACTIVE_FLAG)

    wb.save(response)
    return response

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def getCategoryList():
    categorylist = set()
    p = Product_Master.objects.all()
    for product in p:
        categorylist.add(product.PROD_LEVEL2_DESC)
    return categorylist


@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def thematicPromotionTemplate(request,productcategory):
    productslist = getProductList(productcategory)
    return DisplayPromotionTemplate(request,productslist,'src/thematicpromotion.html',ThematicPromotions_Master.objects.filter(PRODUCT_CATEGORY=productcategory),productcategory,None,'thematic')
    
@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def saveThematicPromotionDate(request):    
    productcategory = request.POST.get('productcategory','')
    objid = request.POST.get('objid','')
    productslist = getProductList(productcategory)
    saveThematicPromotinData(request,productcategory,objid)
    return redirect('/product-details/thematicpromotion/'+productcategory)
    #return DisplayPromotionTemplate(request,productslist,'src/thematicpromotion.html',ThematicPromotions_Master.objects.filter(PRODUCT_CATEGORY=productcategory),productcategory)

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def editThematicPromotionData(request,objid,productcategory):
    productobj = ThematicPromotions_Master.objects.get(id=objid)
    productslist = getProductList(productcategory)
    return DisplayPromotionTemplate(request,productslist,'src/thematicpromotion.html',ThematicPromotions_Master.objects.filter(PRODUCT_CATEGORY=productcategory),productcategory,productobj,'thematic')

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def TacticPromotionTemplate(request,productcategory):
    productslist = getProductList(productcategory)
    return DisplayPromotionTemplate(request,productslist,'src/tacticpromotions.html',TacticalPromotions_Master.objects.filter(PRODUCT_CATEGORY=productcategory),productcategory,None,'tactical')

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def saveTacticPromotionDate(request):
    productcategory = request.POST.get('productcategory','')
    objid = request.POST.get('objid','')
    productslist = getProductList(productcategory)
    saveTacticPromotinData(request,productcategory,objid)
    return redirect('/product-details/tacticpromotion/'+productcategory)
    #return DisplayPromotionTemplate(request,productslist,'src/tacticpromotions.html',ThematicPromotions_Master.objects.filter(PRODUCT_CATEGORY=productcategory),productcategory)

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def editTacticPromotionData(request,objid,productcategory):
    productobj = TacticalPromotions_Master.objects.get(id=objid)
    productslist = getProductList(productcategory)
    return DisplayPromotionTemplate(request,productslist,'src/tacticpromotions.html',TacticalPromotions_Master.objects.filter(PRODUCT_CATEGORY=productcategory),productcategory,productobj,'tactical')

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def internalPromotionTemplate(request,productcategory):
    productslist = getProductList(productcategory)
    return DisplayPromotionTemplate(request,productslist,'src/internalpromotion.html',InternalPromotions_Master.objects.filter(PRODUCT_CATEGORY=productcategory),productcategory,None,'internal')
    
@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def saveInternalPromotionDate(request):    
    productcategory = request.POST.get('productcategory','')
    objid = request.POST.get('objid','')
    productslist = getProductList(productcategory)
    saveInternalPromotinData(request,productcategory,objid)
    return redirect('/product-details/internalpromotion/'+productcategory)

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def editInternalPromotionData(request,objid,productcategory):
    productobj = InternalPromotions_Master.objects.get(id=objid)
    productslist = getProductList(productcategory)
    return DisplayPromotionTemplate(request,productslist,'src/internalpromotion.html',InternalPromotions_Master.objects.filter(PRODUCT_CATEGORY=productcategory),productcategory,productobj,'internal')
   
def getProductList(productcategory):
    if productcategory=='Television':
        productslist = Product_Master.objects.filter(PROD_LEVEL2_DESC__in=['TELEVISIONS - LED','TELEVISION - LCD & PLASMA','TELEVISIONS - CRT'])
    else:
        productslist = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact=productcategory)
    return productslist


@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def DisplayPromotionTemplate(request,productslist,templatename,promotiondata,productcategory,productobj,promotion):
    productcategory = productcategory.split("/")[0] if len(productcategory.split("/"))>0 else productcategory
    provincelist=set();storelist=set();brandlist=set();districtlist = set();mediumlist={"Newspaper","Radio","Others","Television"}
    storelistdata = Store_Master.objects.all()
    for storeitem in storelistdata:
        provincelist.add(storeitem.STORE_PROVINCE)
        districtlist.add(storeitem.DISTRICT)
        storelist.add(storeitem.SITE_ID)
    for productitem in productslist:
        brandlist.add(productitem.BRAND_LEVEL2_DESC)
    return render(request, templatename,{'category_name':getcategoryname(request.user),'provincelist':provincelist,'districtlist':districtlist,
        'storelist':storelist,'brandlist':brandlist,'promotiondata':promotiondata,'productcategory':productcategory,'productobj':productobj,'mediumlist':mediumlist,promotion:'active'})

def saveInternalPromotinData(request,productcategory,objid):
    if objid:
        promotion = InternalPromotions_Master.objects.get(id=objid)
    else:
        promotion = InternalPromotions_Master()
    promotion.PRODUCT_CATEGORY = productcategory
    promotion.FROM_DATE = request.POST.get('fromdate', '') 
    promotion.TO_DATE = request.POST.get('todate', '') 
    promotion.PROVINCE = request.POST.get('province', '') 
    promotion.DISTRICT = request.POST.get('district', '')
    promotion.BRAND = request.POST.get('brand', '') 
    promotion.STAFF = request.POST.get('staff', '') 
    promotion.TYPE = request.POST.get('type', '')
    promotion.EXPECTED_SPEND = request.POST.get('fromdate', '')
    promotion.SKU = request.POST.get('sku','')
    promotion.TARGET_GIVEN = request.POST.get('targetgiven','')
    promotion.EXPECTED_SPEND = request.POST.get('expectedspend','')
    promotion.STORE = request.POST.get('store','')
    try:
        promotion.save()
        if objid:
            messages.success(request, 'Promotion details updated successfully.')
        else:
            messages.success(request, 'Promotion details saved successfully.')
    except:
        messages.success(request, 'Issue in saving promotion details.')

def saveThematicPromotinData(request,productcategory,objid):
    if objid:
        promotion = ThematicPromotions_Master.objects.get(id=objid)
    else:
        promotion = ThematicPromotions_Master()
    promotion.PRODUCT_CATEGORY = productcategory
    promotion.FROM_DATE = request.POST.get('fromdate', '') 
    promotion.TO_DATE = request.POST.get('todate', '') 
    promotion.PROVINCE = request.POST.get('province', '') 
    promotion.DISTRICT = request.POST.get('district', '')
    promotion.BRAND = request.POST.get('brand', '') 
    promotion.MEDIUM = request.POST.get('medium', '')
    promotion.OTHER_MEDIUM = request.POST.get('othermedium', '')
    promotion.TYPE = request.POST.get('type', '')
    promotion.SKU = request.POST.get('sku','')
    promotion.EXPECTED_SPEND = request.POST.get('expectedspend','')
    promotion.STORE = request.POST.get('store','')
    try:
        promotion.save()
        if objid:
            messages.success(request, 'Promotion details updated successfully.')
        else:
            messages.success(request, 'Promotion details saved successfully.')
    except:
        messages.success(request, 'Issue in saving promotion details.')

def saveTacticPromotinData(request,productcategory,objid):
    if objid:
        promotion = TacticalPromotions_Master.objects.get(id=objid)
    else:
        promotion = TacticalPromotions_Master()
    promotion.PRODUCT_CATEGORY = productcategory
    promotion.FROM_DATE = request.POST.get('fromdate', '') 
    promotion.TO_DATE = request.POST.get('todate', '') 
    promotion.PROVINCE = request.POST.get('province', '') 
    promotion.DISTRICT = request.POST.get('district', '')
    promotion.BRAND = request.POST.get('brand', '') 
    promotion.MEDIUM = request.POST.get('medium', '')
    promotion.OTHER_MEDIUM = request.POST.get('othermedium', '')
    promotion.TYPE = request.POST.get('type', '')
    promotion.SKU = request.POST.get('sku','')
    promotion.EXPECTED_SPEND = request.POST.get('expectedspend','')
    promotion.SPEC_INTRST_SCHME = "Yes" if request.POST.get('specialinterestscheme','')=="on" else "No"
    promotion.SCHEME = request.POST.get('scheme','')
    promotion.STORE = request.POST.get('store','')
    try:
        promotion.save()
        if objid:
            messages.success(request, 'Promotion details updated successfully.')
        else:
            messages.success(request, 'Promotion details saved successfully.')
    except:
        messages.success(request, 'Issue in saving promotion details.')

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def delTacticalPromotion(request,objid,productcategory):
    promotion = TacticalPromotions_Master.objects.get(id=objid)
    try:
        promotion.delete()
        messages.success(request, 'Promotion details deleted successfully.')
    except:
        messages.success(request, 'Issue in deleting promotion details.')
    return redirect('/product-details/tacticpromotion/'+productcategory)

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def export_tacticalpromotiondetails_xls(request,productcategory):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "TactialPromotions.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Tactical Promotion Details')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = [ 'From Date', 'to Date', 'District', 'Province', 'Brand', 
               'Store','Sku','Medium','In case of other medium','Type','Expected Spend','Special Interest Scheme','Scheme']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    promotions = TacticalPromotions_Master.objects.filter(PRODUCT_CATEGORY=productcategory)
    for promotion in promotions:
        row_num += 1
        ws.write(row_num, 0, promotion.FROM_DATE)
        ws.write(row_num, 1, promotion.TO_DATE)
        ws.write(row_num, 2, promotion.DISTRICT)
        ws.write(row_num, 3, promotion.PROVINCE)
        ws.write(row_num, 4, promotion.BRAND)
        ws.write(row_num, 5, promotion.STORE)
        ws.write(row_num, 6, promotion.SKU)
        ws.write(row_num, 7, promotion.MEDIUM)
        ws.write(row_num, 8, promotion.OTHER_MEDIUM)        
        ws.write(row_num, 9, promotion.TYPE)
        ws.write(row_num, 10, promotion.EXPECTED_SPEND)
        ws.write(row_num, 11, promotion.SPEC_INTRST_SCHME)
        ws.write(row_num, 12, promotion.SCHEME)

    wb.save(response)
    return response

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def delThematicPromotion(request,objid,productcategory):
    promotion = ThematicPromotions_Master.objects.get(id=objid)
    try:
        promotion.delete()
        messages.success(request, 'Promotion details deleted successfully.')
    except:
        messages.success(request, 'Issue in deleting promotion details.')
    return redirect('/product-details/thematicpromotion/'+productcategory)

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def export_thematicpromotiondetails_xls(request,productcategory):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "ThematicPromotions.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Thematic Promotion Details')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = [ 'From Date', 'to Date', 'District', 'Province', 'Brand', 
               'Store','Sku','Medium','In case of other medium','Type','Expected Spend']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    promotions = ThematicPromotions_Master.objects.filter(PRODUCT_CATEGORY=productcategory)
    for promotion in promotions:
        row_num += 1
        ws.write(row_num, 0, promotion.FROM_DATE)
        ws.write(row_num, 1, promotion.TO_DATE)
        ws.write(row_num, 2, promotion.DISTRICT)
        ws.write(row_num, 3, promotion.PROVINCE)
        ws.write(row_num, 4, promotion.BRAND)
        ws.write(row_num, 5, promotion.STORE)
        ws.write(row_num, 6, promotion.SKU)
        ws.write(row_num, 7, promotion.MEDIUM)
        ws.write(row_num, 8, promotion.OTHER_MEDIUM)        
        ws.write(row_num, 9, promotion.TYPE)
        ws.write(row_num, 10, promotion.EXPECTED_SPEND)

    wb.save(response)
    return response


@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def delInternalPromotion(request,objid,productcategory):
    promotion = InternalPromotions_Master.objects.get(id=objid)
    try:
        promotion.delete()
        messages.success(request, 'Promotion details deleted successfully.')
    except:
        messages.success(request, 'Issue in deleting promotion details.')
    return redirect('/product-details/internalpromotion/'+productcategory)

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def export_internalpromotiondetails_xls(request,productcategory):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "InternalPromotions.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Internal Promotion Details')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = [ 'From Date', 'to Date', 'District', 'Province', 'Brand', 
               'Store','Sku','Staff','Type','Target Given','Expected Spend']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    promotions = InternalPromotions_Master.objects.filter(PRODUCT_CATEGORY=productcategory)
    for promotion in promotions:
        row_num += 1
        ws.write(row_num, 0, promotion.FROM_DATE)
        ws.write(row_num, 1, promotion.TO_DATE)
        ws.write(row_num, 2, promotion.DISTRICT)
        ws.write(row_num, 3, promotion.PROVINCE)
        ws.write(row_num, 4, promotion.BRAND)
        ws.write(row_num, 5, promotion.STORE)
        ws.write(row_num, 6, promotion.SKU)
        ws.write(row_num, 7, promotion.STAFF)
        ws.write(row_num, 8, promotion.TYPE)        
        ws.write(row_num, 9, promotion.TARGET_GIVEN)
        ws.write(row_num, 10, promotion.EXPECTED_SPEND)

    wb.save(response)
    return response


@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def EventsTemplate(request,productcategory):
    productslist = getProductList(productcategory)
    return DisplayEventTemplate(request,productslist,'src/events.html',Events_Master.objects.filter(PRODUCT_CATEGORY=productcategory),productcategory,None)
    
@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def saveEventsData(request):    
    productcategory = request.POST.get('productcategory','')
    objid = request.POST.get('objid','')
    productslist = getProductList(productcategory)
    saveEventData(request,productcategory,objid)
    return redirect('/product-details/events/'+productcategory)
    #return DisplayPromotionTemplate(request,productslist,'src/thematicpromotion.html',ThematicPromotions_Master.objects.filter(PRODUCT_CATEGORY=productcategory),productcategory)

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def editEventsData(request,objid,productcategory):
    eventobj = Events_Master.objects.get(id=objid)
    productslist = getProductList(productcategory)
    return DisplayEventTemplate(request,productslist,'src/events.html',Events_Master.objects.filter(PRODUCT_CATEGORY=productcategory),productcategory,eventobj)

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def delEventsData(request,eventid,productcategory):
    event = Events_Master.objects.get(id=eventid)
    try:
        event.delete()
        messages.success(request, 'Event deleted successfully.')
    except:
        messages.success(request, 'Issue in deleting event details.')
    return redirect('/product-details/events/'+productcategory)

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def export_eventdetails_xls(request,productcategory):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "EventDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Event Details')

    # Sheet header, first row
    row_num = 0

    test_manager = []

    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = [ 'From Date', 'to Date', 'District', 'Province', 'Brand', 'SKU',
               'Store','Type','Positive Impact','Event','In case of others','Expected Impact (in percentage)']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    events = Events_Master.objects.filter(PRODUCT_CATEGORY=productcategory)
    for event in events:
        row_num += 1
        ws.write(row_num, 0, event.FROM_DATE)
        ws.write(row_num, 1, event.TO_DATE)
        ws.write(row_num, 2, event.DISTRICT)
        ws.write(row_num, 3, event.PROVINCE)
        ws.write(row_num, 4, event.BRAND)
        ws.write(row_num, 5, event.SKU)
        ws.write(row_num, 6, event.STORE)
        ws.write(row_num, 7, event.TYPE)
        ws.write(row_num, 8, event.POSITIVE_IMPACT)
        ws.write(row_num, 9, event.EVENT)        
        ws.write(row_num, 10, event.OTHER_EVENT)
        ws.write(row_num, 11, event.EXPECTED_PRCNT_IMPACT)

    wb.save(response)
    return response

def DisplayEventTemplate(request,productslist,templatename,eventdata,productcategory,eventobj):
    productcategory = productcategory.split("/")[0] if len(productcategory.split("/"))>0 else productcategory
    provincelist=set();storelist=set();brandlist=set();districtlist = set();eventlist={"Stockout","Flood","Draught","Bulk Sale","Festival","Others","Focus SKU"}
    storelistdata = Store_Master.objects.all()
    for storeitem in storelistdata:
        provincelist.add(storeitem.STORE_PROVINCE)
        districtlist.add(storeitem.DISTRICT)
        storelist.add(storeitem.SITE_ID)
    for productitem in productslist:
        brandlist.add(productitem.BRAND_LEVEL2_DESC)
    # for eventitem in Events_Master.objects.all():
    #     eventlist.add(eventitem.EVENT)
    return render(request, templatename,{'category_name':getcategoryname(request.user),'provincelist':provincelist,'districtlist':districtlist,
        'storelist':storelist,'brandlist':brandlist,'eventdata':eventdata,'productcategory':productcategory,'eventobj':eventobj,'eventlist':eventlist,'events':'active'})


def saveEventData(request,productcategory,objid):
    if objid:
        event = Events_Master.objects.get(id=objid)
    else:
        event = Events_Master()
    event.PRODUCT_CATEGORY = productcategory
    event.FROM_DATE = request.POST.get('fromdate', '') 
    event.TO_DATE = request.POST.get('todate', '') 
    event.PROVINCE = request.POST.get('province', '') 
    event.DISTRICT = request.POST.get('district', '')
    event.BRAND = request.POST.get('brand', '') 
    event.EVENT = request.POST.get('event', '')
    event.SKU = request.POST.get('sku', '')
    event.OTHER_EVENT = request.POST.get('otherevent', '')
    event.TYPE = request.POST.get('type', '')
    event.POSITIVE_IMPACT = "Yes" if request.POST.get('impact','')=="on" else "No"
    event.STORE = request.POST.get('store','')
    event.EXPECTED_PRCNT_IMPACT = request.POST.get('expectedimpact','')
    try:
        event.save()
        if objid:
            messages.success(request, 'Event details updated successfully.')
        else:
            messages.success(request, 'Event details saved successfully.')
    except:
        messages.success(request, 'Issue in saving event details.')



def getActiveProductsCount(productcategory):
    if productcategory=='Television':
        active = Product_Master.objects.filter(PROD_LEVEL2_DESC__in=['TELEVISIONS - LED','TELEVISION - LCD & PLASMA','TELEVISIONS - CRT'],ACTIVE_FLAG__iexact="Yes").count() 
    else:
        active = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact=productcategory,ACTIVE_FLAG__iexact="Yes").count()
    return active

    
def getInActiveProductsCount(productcategory):
    if productcategory=='Television':
        inactive = Product_Master.objects.filter(PROD_LEVEL2_DESC__in=['TELEVISIONS - LED','TELEVISION - LCD & PLASMA','TELEVISIONS - CRT'],ACTIVE_FLAG__iexact="No").count() 
    else:
        inactive = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact=productcategory,ACTIVE_FLAG__iexact="No").count()
    return inactive

def getNoStatusProductsCount(productcategory):
    if productcategory=='Television':
        nostatus = Product_Master.objects.filter(PROD_LEVEL2_DESC__in=['TELEVISIONS - LED','TELEVISION - LCD & PLASMA','TELEVISIONS - CRT'],ACTIVE_FLAG=None).count() + Product_Master.objects.filter(PROD_LEVEL2_DESC__in=['TELEVISIONS - LED','TELEVISION - LCD & PLASMA','TELEVISIONS - CRT'],ACTIVE_FLAG="").count() + Product_Master.objects.filter(PROD_LEVEL2_DESC__in=['TELEVISIONS - LED','TELEVISION - LCD & PLASMA','TELEVISIONS - CRT'],ACTIVE_FLAG__iexact="#N/A").count()  
    else:
        nostatus = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact=productcategory,ACTIVE_FLAG=None).count() + Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact=productcategory,ACTIVE_FLAG="").count() + Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact=productcategory,ACTIVE_FLAG__iexact="#N/A").count()  
    return nostatus


def getEndOfLifeProductsCount(productcategory):
    if productcategory=='Television':
        endoflife = Product_Master.objects.filter(PROD_LEVEL2_DESC__in=['TELEVISIONS - LED','TELEVISION - LCD & PLASMA','TELEVISIONS - CRT'],CUSTOM_CATEGORY11__iexact="Yes").count() 
    else:
        endoflife = Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact=productcategory,CUSTOM_CATEGORY11__iexact="Yes").count()
    return endoflife


@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def logTemplate(request,productcategory):
    productcategory = productcategory.split("/")[0] if len(productcategory.split("/"))>0 else productcategory
    logs = UpdateLog_Master.objects.filter(PRODUCT_CATEGORY=productcategory)
    return render(request, 'src/updatelog.html',{'category_name':getcategoryname(request.user),'log':'active','logs':logs,'productcategory':productcategory,'todaydate':datetime.today().strftime("%Y-%m-%d")})
    
def saveUpdateLogData(request):
    productcategory = request.POST.get('productcategory','')
    productslist = getProductList(productcategory)
    saveLogData(request,productcategory)
    return redirect('/product-details/update-log/'+productcategory)

def saveLogData(request,productcategory):
    log = UpdateLog_Master()
    log.PRODUCT_CATEGORY = productcategory
    log.MANAGER_NAME = request.POST.get('managername', '') 
    log.TODAY_DATE = datetime.today().strftime("%Y-%m-%d")
    log.PRODUCT_DETAILS_ENTERED = request.POST.get('productdetails', '') 
    log.TACTICAL_ENTERED = request.POST.get('tacticalpromotion', '') 
    log.THEMATIC_ENTERED = request.POST.get('thematicpromotion', '') 
    log.INTERNAL_ENTERED = request.POST.get('internalpromotion', '') 
    log.EVENTS_ENTERED = request.POST.get('events', '') 
    log.EXTERNAL_EVENTS_ENTERED = request.POST.get('externalevents', '') 
    log.INTERNAL_EVENTS_ENTERED = request.POST.get('internalevents', '') 
    try:
        log.save()
        messages.success(request, 'Log details saved successfully.')
    except:
        messages.success(request, 'Issue in saving log details.')

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def export_logdetails_xls(request,productcategory):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "LogDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Log Details')
    row_num = 0
    test_manager = []
    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Category', 'Manager Name', 'Date', 'Product Details Entered', 'Tactical Promotions Entered', 'Thematic Promotions Entered', 
               'Internal Promotions Entered', 'Events Entered','Post Month Events Entered(External)','Post Month Events(Internal)']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    logs = UpdateLog_Master.objects.filter(PRODUCT_CATEGORY=productcategory)
    for log in logs:
        row_num += 1
        ws.write(row_num, 0, log.PRODUCT_CATEGORY)
        ws.write(row_num, 1, log.MANAGER_NAME)
        ws.write(row_num, 2, str(log.TODAY_DATE))
        ws.write(row_num, 3, log.PRODUCT_DETAILS_ENTERED)
        ws.write(row_num, 4, log.TACTICAL_ENTERED)
        ws.write(row_num, 5, log.THEMATIC_ENTERED)
        ws.write(row_num, 6, log.INTERNAL_ENTERED)
        ws.write(row_num, 7, log.EVENTS_ENTERED)
        ws.write(row_num, 8, log.EXTERNAL_EVENTS_ENTERED)
        ws.write(row_num, 9, log.INTERNAL_EVENTS_ENTERED)

    wb.save(response)
    return response

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def singerPromotionTemplate(request,productcategory):
    productcategory = productcategory.split("/")[0] if len(productcategory.split("/"))>0 else productcategory
    if productcategory=='Television':
        promotions = promotions_product.objects.filter(PROD_LEVEL2_DESC__in=['TELEVISIONS - LED','TELEVISION - LCD & PLASMA','TELEVISIONS - CRT']) 
    else:
        promotions = promotions_product.objects.filter(PROD_LEVEL2_DESC__iexact=productcategory)
    return render(request, 'src/singer-promotions.html',{'category_name':getcategoryname(request.user),'promotionstab':'active','promotions':promotions,'productcategory':productcategory})

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def export_singerpromotion_details_xls(request,productcategory):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "PromotionsDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Promotions Details')
    row_num = 0
    test_manager = []
    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Promotion Code', 'Promotion Description',  'Promotion Start Date','Promotion End Date',
    'Promotion Free Item','Product Code','Product Category']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    if productcategory=='Television':
        promotions = promotions_product.objects.filter(PROD_LEVEL2_DESC__in=['TELEVISIONS - LED','TELEVISION - LCD & PLASMA','TELEVISIONS - CRT']) 
    else:
        promotions = promotions_product.objects.filter(PROD_LEVEL2_DESC__iexact=productcategory)
    
    for promotion in promotions:
        row_num += 1
        ws.write(row_num, 0, promotion.PROMO_CODE)
        ws.write(row_num, 1, promotion.PROMO_DESC)
        ws.write(row_num, 2, str(promotion.PROMO_ST_DATE))
        ws.write(row_num, 3, str(promotion.PROMO_END_DATE))
        ws.write(row_num, 4, promotion.PROMOTION_FREE_ITEM)
        ws.write(row_num, 5, promotion.PROMOTION_EFFECT_MAIN_ITEM)
        ws.write(row_num, 6, promotion.PROD_LEVEL2_DESC)

    wb.save(response)
    return response

def uploadforecast(request,productcategory):
    today = datetime.now()
    #forecast_data_exist = "false"
    if request.method=="POST":
        upload_csv_file = request.FILES["csv_file"]
        filename = upload_csv_file.name
        if not filename.endswith('.csv'):
            messages.error(request,'File is not CSV type')
            return redirect('/product-details/uploadforecast/'+productcategory)
        #if file is too large, return
        if upload_csv_file.multiple_chunks():
            messages.error(request,"Uploaded file is too big (%.2f MB)." % (csv_file.size/(1000*1000),))
            return redirect('/product-details/uploadforecast/'+productcategory)

        file_data = upload_csv_file.read().decode("utf-8")     

        lines = file_data.split("\n")
        line_count = 0
        for line in lines:                      
            row = line.split(",")
            if line_count == 0:
                pass
            else:
                if len(row)>1:
                    part_number = str(row[0])
                    year = row[1]
                    month = row[2]
                    forecast_qty = row[3]
                    upload_mon_year = str(today.year)+":"+str(today.month)
                    try:
                        forecast_list = forecast_table.objects.filter(PRODUCT_CATEGORY=productcategory,PART_NUMBER=part_number,YEAR=year,MONTH=month,FORECAST_QTY=forecast_qty,UPLOAD_YEAR_MONTH=upload_mon_year)
                        if forecast_list.count()==0:
                            forecast_table.objects.create(PRODUCT_CATEGORY=productcategory,PART_NUMBER=part_number,YEAR=year,MONTH=month,FORECAST_QTY=forecast_qty,UPLOAD_YEAR_MONTH=upload_mon_year)
                        else:
                            forecast_table.objects.filter(PRODUCT_CATEGORY=productcategory,PART_NUMBER=part_number,YEAR=year,MONTH=month,UPLOAD_YEAR_MONTH=upload_mon_year).update(FORECAST_QTY=forecast_qty)
                    except:
                        messages.error(request,'Error in saving the data, please check the csv file')
            line_count = line_count+1
        messages.success(request,'Saved Successfully')
    #if request.method=="GET":
    #forecastdata = forecast_table.objects.filter(PRODUCT_CATEGORY__iexact=productcategory)
    return render(request, 'src/upload_forecast.html',{'category_name':getcategoryname(request.user),'uploadforecast':'active','productcategory':productcategory})

def getcategoryname(username):
    userobj = User.objects.get(username=username)
    category_name = userobj.first_name
    return category_name

def getreplacementcodelist(productcategory):
    if productcategory=="Television":
        replacement_code_list = set(Product_Master.objects.filter(PROD_LEVEL2_DESC__in=['TELEVISIONS - LED','TELEVISION - LCD & PLASMA','TELEVISIONS - CRT']).values_list('PRODUCT_CODE',flat=True))
    else:  
        replacement_code_list = set(Product_Master.objects.filter(PROD_LEVEL2_DESC__iexact=productcategory).values_list('PRODUCT_CODE',flat=True))
    return replacement_code_list

@cache_control(no_cache=True, must_revalidate=True)
@login_required(login_url='/accounts/login/')
def export_forecastdata_xls(request,productcategory):
    response = HttpResponse(content_type='application/ms-excel')
    filename = "ForecastDetails.xls"
    response['Content-Disposition'] = 'attachment; filename=' + filename
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Log Details')
    row_num = 0
    test_manager = []
    font_style = xlwt.XFStyle()
    font_style = xlwt.easyxf('pattern: pattern solid, fore_colour light_blue;'
                             'font: colour white, bold True;')

    columns = ['Product Category', 'Part Number', 'Year', 'Month', 'Forecast Qty', 'Uploaded Year:Month']

    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
        ws.col(col_num).width = len(columns[0]) * 356
        
    forecastdata = forecast_table.objects.filter(PRODUCT_CATEGORY=productcategory)
    for forecastobj in forecastdata:
        row_num += 1
        ws.write(row_num, 0, forecastobj.PRODUCT_CATEGORY)
        ws.write(row_num, 1, forecastobj.PART_NUMBER)
        ws.write(row_num, 2, forecastobj.YEAR)
        ws.write(row_num, 3, forecastobj.MONTH)
        ws.write(row_num, 4, forecastobj.FORECAST_QTY)
        ws.write(row_num, 5, forecastobj.UPLOAD_YEAR_MONTH)

    wb.save(response)
    return response

class forecast_table_asJson(BaseDatatableView):
    model = forecast_table

    columns = ['PART_NUMBER','YEAR','MONTH','FORECAST_QTY','UPLOAD_YEAR_MONTH']

    def filter_queryset(self, qs):
        print(self.request.GET.get('productcategory',None))
        productcategory = self.request.GET.get('productcategory',None)
        qs = qs.filter(PRODUCT_CATEGORY__iexact=productcategory)
        # warehouse = Users_Warehouse_Mapping.objects.filter(user__username=self.request.user).values_list('warehouse',flat=True)
        # warehouse = list(warehouse)
        # qs = qs.filter(warehouse__in=warehouse)
        return qs