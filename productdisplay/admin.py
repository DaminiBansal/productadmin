from django.contrib import admin
from .models import forecast_table,Product_Master,ThematicPromotions_Master,Store_Master,TacticalPromotions_Master,Events_Master,UpdateLog_Master

# Register your models here.
class ProductAdmin(admin.ModelAdmin):
    model = Product_Master
    #resource_class = DataResource
    #
    search_fields = ('PRODUCT_CODE','PROD_LEVEL2_DESC')
    list_display = ('PRODUCT_CODE','PROD_LEVEL2_DESC', )
admin.site.register(Product_Master,ProductAdmin)

class ThematicPromotionsAdmin(admin.ModelAdmin):
    model = ThematicPromotions_Master
    search_fields = ('PRODUCT_CATEGORY','SKU')
    list_display = ('PRODUCT_CATEGORY','SKU', )
admin.site.register(ThematicPromotions_Master,ThematicPromotionsAdmin)

class TacticPromotionsAdmin(admin.ModelAdmin):
    model = ThematicPromotions_Master
    search_fields = ('PRODUCT_CATEGORY','SKU')
    list_display = ('PRODUCT_CATEGORY','SKU', )
admin.site.register(TacticalPromotions_Master,TacticPromotionsAdmin)

class StoreAdmin(admin.ModelAdmin):
    model = Store_Master
    search_fields = ('SITE_ID',)
    list_display = ('SITE_ID', )
admin.site.register(Store_Master,StoreAdmin)

class EventAdmin(admin.ModelAdmin):
    model = Events_Master
    search_fields = ('PRODUCT_CATEGORY','EVENT')
    list_display = ('PRODUCT_CATEGORY','EVENT', )
admin.site.register(Events_Master,EventAdmin)
admin.site.register(UpdateLog_Master)

class ForecastAdmin(admin.ModelAdmin):
    model = forecast_table
    search_fields = ('PRODUCT_CATEGORY','PART_NUMBER')
    list_display = ('PRODUCT_CATEGORY','PART_NUMBER', )
admin.site.register(forecast_table,ForecastAdmin)