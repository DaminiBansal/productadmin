from django.db import models

# Create your models here.

# class Login_Master(models.Model):
#   username  = models.CharField(primary_key=True,max_length=100),
#   password  = models.CharField(max_length=100)

# class level(models.Model):
#     username  = models.CharField(primary_key=True,max_length=100),
#     password  = models.CharField(max_length=100)

class Product_Master(models.Model):
    PRODUCT_CODE         = models.CharField(primary_key=True,max_length=200)
    PRODUCT_DESCRIPTION  = models.CharField(max_length=200,blank=True, null=True, default="")
    PROD_LEVEL1_CODE     = models.CharField(max_length=200,blank=True, null=True, default="")
    PROD_LEVEL1_DESC     = models.CharField(max_length=200,blank=True, null=True, default="")
    PROD_LEVEL2_CODE     = models.CharField(max_length=200,blank=True, null=True, default="")
    PROD_LEVEL2_DESC     = models.CharField(max_length=200,blank=True, null=True, default="")
    PROD_LEVEL3_CODE     = models.CharField(max_length=200,blank=True, null=True, default="")
    PROD_LEVEL3_DESC     = models.CharField(max_length=200,blank=True, null=True, default="")
    BRAND_LEVEL1_CODE    = models.CharField(max_length=200,blank=True, null=True, default="")
    BRAND_LEVEL1_DESC    = models.CharField(max_length=200,blank=True, null=True, default="")
    BRAND_LEVEL2_CODE    = models.CharField(max_length=200,blank=True, null=True, default="")
    BRAND_LEVEL2_DESC    = models.CharField(max_length=200,blank=True, null=True, default="")
    ACTIVE_FLAG          = models.CharField(max_length=200,blank=True, null=True, default="")
    CUSTOM_CATEGORY1     = models.CharField(max_length=200,blank=True, null=True, default="")
    CUSTOM_CATEGORY2     = models.CharField(max_length=200,blank=True, null=True, default="")
    CUSTOM_CATEGORY3     = models.CharField(max_length=200,blank=True, null=True, default="")
    CUSTOM_CATEGORY4     = models.CharField(max_length=200,blank=True, null=True, default="")
    CUSTOM_CATEGORY5     = models.CharField(max_length=200,blank=True, null=True, default="")
    CUSTOM_CATEGORY6     = models.CharField(max_length=200,blank=True, null=True, default="")
    CUSTOM_CATEGORY7     = models.CharField(max_length=200,blank=True, null=True, default="")
    CUSTOM_CATEGORY8     = models.CharField(max_length=200,blank=True, null=True, default="")
    CUSTOM_CATEGORY9     = models.CharField(max_length=200,blank=True, null=True, default="")
    CUSTOM_CATEGORY10    = models.CharField(max_length=200,blank=True, null=True, default="")
    CUSTOM_CATEGORY11    = models.CharField(max_length=200,blank=True, null=True, default="")

class ThematicPromotions_Master(models.Model):
    PRODUCT_CATEGORY = models.CharField(max_length=200,blank=True, null=True, default="")
    FROM_DATE        = models.CharField(max_length=200,blank=True, null=True, default="")
    TO_DATE          = models.CharField(max_length=200,blank=True, null=True, default="")
    DISTRICT         = models.CharField(max_length=200,blank=True, null=True, default="")
    PROVINCE         = models.CharField(max_length=200,blank=True, null=True, default="")
    BRAND            = models.CharField(max_length=200,blank=True, null=True, default="")
    STORE            = models.CharField(max_length=200,blank=True, null=True, default="")
    SKU              = models.CharField(max_length=200,blank=True, null=True, default="ALL")
    MEDIUM           = models.CharField(max_length=200,blank=True, null=True, default="")
    OTHER_MEDIUM     = models.CharField(max_length=500,blank=True, null=True, default="")
    TYPE             = models.CharField(max_length=200,blank=True, null=True, default="")
    EXPECTED_SPEND   = models.CharField(max_length=200,blank=True, null=True, default="")

class TacticalPromotions_Master(models.Model):
    PRODUCT_CATEGORY = models.CharField(max_length=200,blank=True, null=True, default="")
    FROM_DATE        = models.CharField(max_length=200,blank=True, null=True, default="")
    TO_DATE          = models.CharField(max_length=200,blank=True, null=True, default="")
    DISTRICT         = models.CharField(max_length=200,blank=True, null=True, default="")
    PROVINCE         = models.CharField(max_length=200,blank=True, null=True, default="")
    BRAND            = models.CharField(max_length=200,blank=True, null=True, default="")
    STORE            = models.CharField(max_length=200,blank=True, null=True, default="")
    SKU              = models.CharField(max_length=200,blank=True, null=True, default="ALL")
    MEDIUM           = models.CharField(max_length=200,blank=True, null=True, default="")
    OTHER_MEDIUM     = models.CharField(max_length=500,blank=True, null=True, default="")
    TYPE             = models.CharField(max_length=200,blank=True, null=True, default="")
    EXPECTED_SPEND   = models.CharField(max_length=200,blank=True, null=True, default="")   
    SPEC_INTRST_SCHME= models.CharField(max_length=200,blank=True, null=True, default="")
    SCHEME           = models.CharField(max_length=1000,blank=True, null=True, default="")

class Events_Master(models.Model):
    PRODUCT_CATEGORY = models.CharField(max_length=200,blank=True, null=True, default="")
    FROM_DATE        = models.CharField(max_length=200,blank=True, null=True, default="")
    TO_DATE          = models.CharField(max_length=200,blank=True, null=True, default="")
    DISTRICT         = models.CharField(max_length=200,blank=True, null=True, default="")
    PROVINCE         = models.CharField(max_length=200,blank=True, null=True, default="")
    BRAND            = models.CharField(max_length=200,blank=True, null=True, default="")
    STORE            = models.CharField(max_length=200,blank=True, null=True, default="")
    TYPE             = models.CharField(max_length=200,blank=True, null=True, default="")
    POSITIVE_IMPACT   = models.CharField(max_length=200,blank=True, null=True, default="")  
    EVENT             = models.CharField(max_length=200,blank=True, null=True, default="")
    OTHER_EVENT      = models.CharField(max_length=500,blank=True, null=True, default="")
    EXPECTED_PRCNT_IMPACT  = models.CharField(max_length=1000,blank=True, null=True, default="")
    SKU              = models.CharField(max_length=200,blank=True, null=True, default="ALL")

class InternalPromotions_Master(models.Model):
    PRODUCT_CATEGORY = models.CharField(max_length=200,blank=True, null=True, default="")
    FROM_DATE        = models.CharField(max_length=200,blank=True, null=True, default="")
    TO_DATE          = models.CharField(max_length=200,blank=True, null=True, default="")
    DISTRICT         = models.CharField(max_length=200,blank=True, null=True, default="")
    PROVINCE         = models.CharField(max_length=200,blank=True, null=True, default="")
    BRAND            = models.CharField(max_length=200,blank=True, null=True, default="")
    STORE            = models.CharField(max_length=200,blank=True, null=True, default="")
    SKU              = models.CharField(max_length=200,blank=True, null=True, default="ALL")
    STAFF            = models.CharField(max_length=200,blank=True, null=True, default="")
    TYPE             = models.CharField(max_length=200,blank=True, null=True, default="")
    TARGET_GIVEN     = models.CharField(max_length=200,blank=True, null=True, default="")
    EXPECTED_SPEND   = models.CharField(max_length=200,blank=True, null=True, default="")

class Store_Master(models.Model):
    row_names        = models.AutoField(primary_key=True)
    SITE_ID          = models.CharField(max_length=200,blank=True, null=True, default="")
    CHANNEL_ID       = models.IntegerField(blank=True, null=True)
    CHANNEL          = models.CharField(max_length=200,blank=True, null=True, default="")
    STORE_PROVINCE   = models.CharField(max_length=200,blank=True, null=True, default="")
    BRANCH           = models.CharField(max_length=200,blank=True, null=True, default="")
    DISTRICT         = models.CharField(max_length=200,blank=True, null=True, default="")
    TYPE             = models.CharField(max_length=200,blank=True, null=True, default="")
    LATTITUDE        = models.CharField(max_length=200,blank=True, null=True, default="")
    LONGITUDE        = models.CharField(max_length=200,blank=True, null=True, default="")
    MOBILE_NO        = models.CharField(max_length=200,blank=True, null=True, default="")

class UpdateLog_Master(models.Model):
    PRODUCT_CATEGORY                = models.CharField(max_length=200)
    MANAGER_NAME                    = models.CharField(max_length=300,blank=True, null=True, default="")
    TODAY_DATE                      = models.DateField()
    PRODUCT_DETAILS_ENTERED         = models.CharField(max_length=5,default="yes")
    TACTICAL_ENTERED                = models.CharField(max_length=5,default="yes")
    THEMATIC_ENTERED                = models.CharField(max_length=5,default="yes")
    INTERNAL_ENTERED                = models.CharField(max_length=5,default="yes")
    EVENTS_ENTERED                  = models.CharField(max_length=5,default="yes")
    EXTERNAL_EVENTS_ENTERED         = models.CharField(max_length=5,default="yes")
    INTERNAL_EVENTS_ENTERED         = models.CharField(max_length=5,default="yes")
    
class promotions_product(models.Model):
    PROMO_CODE                  = models.AutoField(primary_key=True)
    PROMO_DESC                  = models.CharField(max_length=300)
    PROMO_ST_DATE               = models.DateTimeField()
    PROMO_END_DATE              = models.DateTimeField()
    PROMOTION_FREE_ITEM         = models.CharField(max_length=500)
    PROMOTION_EFFECT_MAIN_ITEM  = models.CharField(max_length=300)
    PROD_LEVEL2_DESC             = models.CharField(max_length=300)

    class Meta:
        db_table = 'promotions_product' 

class forecast_table(models.Model):
    PRODUCT_CATEGORY                = models.CharField(max_length=200)
    PART_NUMBER                     = models.CharField(max_length=50)
    YEAR                            = models.CharField(max_length=5)
    MONTH                           = models.CharField(max_length=5)
    FORECAST_QTY                    = models.CharField(max_length=20)
    UPLOAD_YEAR_MONTH                 = models.CharField(max_length=10)
