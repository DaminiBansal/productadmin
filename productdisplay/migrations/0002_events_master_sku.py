# Generated by Django 2.1.3 on 2019-02-09 19:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('productdisplay', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='events_master',
            name='SKU',
            field=models.CharField(blank=True, default='ALL', max_length=200, null=True),
        ),
    ]
