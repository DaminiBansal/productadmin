# Generated by Django 2.1.7 on 2019-07-20 19:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('productdisplay', '0004_promotions_product'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='promotions_product',
            name='PROMO_ENTER_DATE',
        ),
    ]
