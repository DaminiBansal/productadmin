from django.core.management.base import BaseCommand, CommandParser
import argparse
from django.core.management import BaseCommand
import csv
from django.template import Context, loader
from ...models import Product_Master
from django.core.exceptions import ObjectDoesNotExist

class Command(BaseCommand): 

    def handle(self, *args, **options):
        self.importdata()
        


    def importdata(self):
        with open('importerrordata.csv', 'w') as f2:
            with open('data.csv', 'r',encoding='utf-8') as f:
                reader = csv.reader(f)
                next(reader)
                for row in reader:
                    data = Product_Master()
                    print("enter")
                    try:
                        data.PRODUCT_CODE                = row[0]
                        data.PRODUCT_DESCRIPTION         = row[1]
                        data.PROD_LEVEL1_CODE            = row[2]              
                        data.PROD_LEVEL1_DESC            = row[3]
                        data.PROD_LEVEL2_CODE            = row[4]
                        data.PROD_LEVEL2_DESC            = row[5]
                        data.PROD_LEVEL3_CODE            = row[6]
                        data.PROD_LEVEL3_DESC            = row[7]
                        data.BRAND_LEVEL1_CODE           = row[8]
                        data.BRAND_LEVEL1_DESC           = row[9]
                        data.BRAND_LEVEL2_CODE           = row[10]
                        data.BRAND_LEVEL2_DESC           = row[11]
                        data.ACTIVE_FLAG                 = row[12]
                        data.CUSTOM_CATEGORY1            = row[13]
                        data.CUSTOM_CATEGORY2            = row[14]
                        try:
                           print("added")
                           data.save()
                        except Exception as e:
                           writer = csv.writer(f2,delimiter =",",quoting=csv.QUOTE_MINIMAL)
                           writer.writerow([str(e),row[2]])
                

                    except Exception as e:
                        writer = csv.writer(f2,delimiter =",",quoting=csv.QUOTE_MINIMAL)
                        writer.writerow([str(e),row])                

