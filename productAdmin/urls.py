"""productAdmin URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import include, url 
from django.urls import path

from django.contrib.auth import views 
from productdisplay.views import (login,updateRefrigeratorDetails,refrigeratorEditForm,
    televisionEditForm,updateTelevisionDetails,
    kitchenappliancesEditForm,updateKitchenappliancesDetails,
    airconditionerEditForm,updateairconditionerDetails,
    fanEditForm,updatefanDetails,
    ironEditForm,updateironDetails,
    washingmachineEditForm,updatewashingmachineDetails,
    ricecookerEditForm,updatericecookerDetails,
    audioEditForm,updateaudioDetails,
    sewingmachineEditForm,updatesewingmachineDetails,
    waterpumpEditForm,updatewaterpumpDetails,
    export_refrigerator_productdetails_xls,export_kitchenappliance_productdetails_xls,export_television_productdetails_xls,
    export_airconditioner_productdetails_xls,export_fan_productdetails_xls,export_iron_productdetails_xls,
    export_washingmachine_productdetails_xls,export_ricecooker_productdetails_xls,export_audio_productdetails_xls,
    export_sewingmachine_productdetails_xls,export_waterpump_productdetails_xls,storeDetail,storeEditForm,updateStoreDetails,
    thematicPromotionTemplate,saveThematicPromotionDate,TacticPromotionTemplate,saveTacticPromotionDate,
    internalPromotionTemplate,saveInternalPromotionDate,saveEventsData,EventsTemplate,
    editInternalPromotionData,editThematicPromotionData,editTacticPromotionData,editEventsData,addUser,saveNewUser,deleteuser,
    edituser,delTacticalPromotion,delThematicPromotion,delInternalPromotion,delEventsData,
    export_tacticalpromotiondetails_xls,export_internalpromotiondetails_xls,export_thematicpromotiondetails_xls,export_eventdetails_xls,
    awsdashboard,
    productDetail,logTemplate,saveUpdateLogData,export_logdetails_xls,singerPromotionTemplate,export_singerpromotion_details_xls,
    uploadforecast,export_forecastdata_xls,forecast_table_asJson)
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required

handler404 = 'productdisplay.views.handler404'
handler500 = 'productdisplay.views.handler500'
urlpatterns = [
    path('sladmin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    url(r'^$', storeDetail,name='storeDetail'),
    url(r'^add-user/$', addUser,name='addUser'),
    url(r'^save-new-user/$', saveNewUser,name='saveNewUser'),
    url(r'^delete-user/(.*)/$', deleteuser,name='deleteuser'),
    url(r'^edit-user/(.*)/$', edituser,name='edituser'),
    url(r'^edit-form/(.*)/$', storeEditForm,name='storeEditForm'),
    url(r'^update-details/(.*)/$', updateStoreDetails,name='updateStoreDetails'),
    url(r'^awsdashboard/$',awsdashboard,name='awsdashboard'),
    url(r'^product-details/thematicpromotion/delete/(.*)/(.*)/$', delThematicPromotion,name ='delThematicPromotion'),
    url(r'^product-details/thematicpromotion/save/$', saveThematicPromotionDate,name ='saveThematicPromotionDate'),
    url(r'^product-details/thematicpromotion/edit/(.*)/(.*)/$', editThematicPromotionData,name ='editThematicPromotionData'),
    url(r'^product-details/thematicpromotion/export/(.*)/$', export_thematicpromotiondetails_xls,name ='export_thematicpromotiondetails_xls'),
    url(r'^product-details/thematicpromotion/(.*)/$', thematicPromotionTemplate,name ='thematicPromotionTemplate'),

    url(r'^product-details/update-log/save/$', saveUpdateLogData,name ='saveUpdateLogData'),
    url(r'^product-details/update-log/export/(.*)/$', export_logdetails_xls,name ='export_logdetails_xls'),
    url(r'^product-details/update-log/(.*)/$', logTemplate,name ='logTemplate'),

    url(r'^product-details/uploadforecast/(.*)/$', uploadforecast,name ='uploadforecast'),
    
    url(r'^product-details/tacticpromotion/delete/(.*)/(.*)/$', delTacticalPromotion,name ='delTacticalPromotion'),
    url(r'^product-details/tacticpromotion/save/$', saveTacticPromotionDate,name ='saveTacticPromotionDate'),
    url(r'^product-details/tacticpromotion/edit/(.*)/(.*)/$', editTacticPromotionData,name ='editTacticPromotionData'),
    url(r'^product-details/tacticpromotion/export/(.*)/$', export_tacticalpromotiondetails_xls,name ='export_tacticalpromotiondetails_xls'),
    url(r'^product-details/tacticpromotion/(.*)/$', TacticPromotionTemplate,name ='TacticPromotionTemplate'),
    
    url(r'^product-details/internalpromotion/delete/(.*)/(.*)/$', delInternalPromotion,name ='delInternalPromotion'),
    url(r'^product-details/internalpromotion/save/$', saveInternalPromotionDate,name ='saveInternalPromotionDate'),
    url(r'^product-details/internalpromotion/edit/(.*)/(.*)/$', editInternalPromotionData,name ='editInternalPromotionData'),
    url(r'^product-details/internalpromotion/export/(.*)/$', export_internalpromotiondetails_xls,name ='export_internalpromotiondetails_xls'),
    url(r'^product-details/internalpromotion/(.*)/$', internalPromotionTemplate,name ='internalPromotionTemplate'),

    url(r'^product-details/events/save/$', saveEventsData,name ='saveEventsData'),
    url(r'^product-details/events/export/(.*)/$', export_eventdetails_xls,name ='export_eventdetails_xls'),
    url(r'^product-details/events/edit/(.*)/(.*)/$', editEventsData,name ='editEventsData'),
    url(r'^product-details/events/delete/(.*)/(.*)/$', delEventsData,name ='delEventsData'),
    url(r'^product-details/events/(.*)/$', EventsTemplate,name ='EventsTemplate'),

    url(r'^product-details/singer-promotions/export/(.*)/$', export_singerpromotion_details_xls,name ='export_singerpromotion_details_xls'),
    url(r'^product-details/singer-promotions/(.*)/$', singerPromotionTemplate,name ='singerPromotionTemplate'),
    
    url(r'^product-details/refrigerators/editform/(.*)/(.*)/$', refrigeratorEditForm,name ='refrigeratorEditForm'),
    url(r'^product-details/refrigerators/update-details/(.*)/$',updateRefrigeratorDetails, name='updateRefrigeratorDetails'),
    url(r'^product-details/television/editform/(.*)/(.*)/$', televisionEditForm,name ='televisionEditForm'),
    url(r'^product-details/television/update-details/(.*)/$',updateTelevisionDetails, name='updateTelevisionDetails'),
    url(r'^product-details/kitchenappliances/editform/(.*)/(.*)/$', kitchenappliancesEditForm,name ='kitchenappliancesEditForm'),
    url(r'^product-details/kitchenappliances/update-details/(.*)/$',updateKitchenappliancesDetails, name='updateKitchenappliancesDetails'),
    url(r'^product-details/airconditioner/editform/(.*)/(.*)/$', airconditionerEditForm,name ='airconditionerEditForm'),
    url(r'^product-details/airconditioner/update-details/(.*)/$',updateairconditionerDetails, name='updateairconditionerDetails'),
    url(r'^product-details/fan/editform/(.*)/(.*)/$', fanEditForm,name ='fanEditForm'),
    url(r'^product-details/fan/update-details/(.*)/$',updatefanDetails, name='updatefanDetails'),
    url(r'^product-details/iron/editform/(.*)/(.*)/$', ironEditForm,name ='ironEditForm'),
    url(r'^product-details/iron/update-details/(.*)/$',updateironDetails, name='updateironDetails'),
    url(r'^product-details/washingmachine/editform/(.*)/(.*)/$', washingmachineEditForm,name ='washingmachineEditForm'),
    url(r'^product-details/washingmachine/update-details/(.*)/$',updatewashingmachineDetails, name='updatewashingmachineDetails'),
    url(r'^product-details/ricecooker/editform/(.*)/(.*)/$', ricecookerEditForm,name ='ricecookerEditForm'),
    url(r'^product-details/ricecooker/update-details/(.*)/$',updatericecookerDetails, name='updatericecookerDetails'),
    url(r'^product-details/audio/editform/(.*)/(.*)/$', audioEditForm,name ='audioEditForm'),
    url(r'^product-details/audio/update-details/(.*)/$',updateaudioDetails, name='updateaudioDetails'),
    url(r'^product-details/sewingmachine/editform/(.*)/(.*)/$', sewingmachineEditForm,name ='sewingmachineEditForm'),
    url(r'^product-details/sewingmachine/update-details/(.*)/$',updatesewingmachineDetails, name='updatesewingmachineDetails'),
    url(r'^product-details/waterpump/editform/(.*)/(.*)/$', waterpumpEditForm,name ='waterpumpEditForm'),
    url(r'^product-details/waterpump/update-details/(.*)/$',updatewaterpumpDetails, name='updatewaterpumpDetails'),

    url(r'^product-details/refrigerators/export$', export_refrigerator_productdetails_xls,name ='export_refrigerator_productdetails_xls'),
    url(r'^product-details/television/export$', export_television_productdetails_xls,name ='export_television_productdetails_xls'),
    url(r'^product-details/kitchenappliances/export$', export_kitchenappliance_productdetails_xls,name ='export_kitchenappliance_productdetails_xls'),
    url(r'^product-details/airconditioner/export$', export_airconditioner_productdetails_xls,name ='export_airconditioner_productdetails_xls'),
    url(r'^product-details/fan/export$', export_fan_productdetails_xls,name ='export_fan_productdetails_xls'),
    url(r'^product-details/iron/export$', export_iron_productdetails_xls,name ='export_iron_productdetails_xls'),
    url(r'^product-details/washingmachine/export$', export_washingmachine_productdetails_xls,name ='export_washingmachine_productdetails_xls'),
    url(r'^product-details/ricecooker/export$', export_ricecooker_productdetails_xls,name ='export_ricecooker_productdetails_xls'),
    url(r'^product-details/audio/export$', export_audio_productdetails_xls,name ='export_audio_productdetails_xls'),
    url(r'^product-details/sewingmachine/export$', export_sewingmachine_productdetails_xls,name ='export_sewingmachine_productdetails_xls'),
    url(r'^product-details/waterpump/export$', export_waterpump_productdetails_xls,name ='export_waterpump_productdetails_xls'),
    url(r'^product-details/forecastdetail/export/(.*)/$', export_forecastdata_xls,name ='export_forecastdata_xls'),
    
    url(r'^forecast_table_asJson/$', login_required(forecast_table_asJson.as_view()),name='forecast_table_asJson'),
    
    url(r'^product-details/(.*)/$', productDetail,name ='productDetail'),
    
    
    
    url(r'^login/$', login, name='login')
    
]
