"""
WSGI config for productAdmin project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/howto/deployment/wsgi/
"""

import os
import re
import sys
import dotenv
from django.core.wsgi import get_wsgi_application

try:
    with open(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), '.env')) as f:
        content = f.read()
except IOError:
        content = ''
        for line in content.splitlines():
            m1 = re.match(r'\A([A-Za-z_0-9]+)=(.*)\Z', line)
            if m1:
                key, val = m1.group(1), m1.group(2)
            m2 = re.match(r"\A'(.*)'\Z", val)
            if m2:
                val = m2.group(1)
            os.environ.setdefault(key, val)


dotenv.read_dotenv(os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), '.env'))
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'productAdmin.settings')

application = get_wsgi_application()
